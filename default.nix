{ pkgs ? import <nixpkgs> {}, ...} :
pkgs.stdenv.mkDerivation {
    name = "toes-env";
    buildInputs = [ pkgs.nodejs pkgs.elmPackages.elm pkgs.elmPackages.elm-format pkgs.gmp ];
    shellHook = ''
        set -e
        npm install
        npm uninstall elm
        patchelf --set-interpreter ${pkgs.stdenv.glibc}/lib/ld-linux-x86-64.so.2 ./node_modules/elm-test/bin/elm-interface-to-json
        patchelf --set-rpath ${pkgs.gmp}/lib ./node_modules/elm-test/bin/elm-interface-to-json
        set +e
    '';
}
