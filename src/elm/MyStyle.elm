module MyStyle exposing (..)

import Html exposing (Attribute)
import Html.Attributes as A
import Css exposing (..)
import Window exposing (Size)
import TicTacToe exposing (Player(..), Position(..), ifX)


(=>) : a -> b -> ( a, b )
(=>) =
    (,)


sty : List Style -> Attribute msg
sty =
    Css.asPairs >> A.style


ifWide : Size -> a -> a -> a
ifWide { width, height } then_ else_ =
    if (toFloat width) / (toFloat height) > 16 / 10 then
        then_
    else
        else_


mainAttrs : List (Attribute msg)
mainAttrs =
    [ A.class "teal lighten-4"
    , sty
        [ height (vh 100)
        , overflow (auto)
        ]
    ]


playerBoxAttrs : Size -> List (Attribute msg)
playerBoxAttrs size =
    [ A.class (ifWide size "col s12" "col s12 m6"), sty [ noBottom ] ]


playerBoxCardAttrs : List (Attribute msg)
playerBoxCardAttrs =
    [ A.class "valign-wrapper card-content white-text"
    , sty [ padding (px 10), noBottom ]
    ]


playerBoxEdgeAttrs : List (Attribute msg)
playerBoxEdgeAttrs =
    [ A.class "col s2 center-align", sty [ fontSize (px 24) ] ]


playerBoxContentAttrs : List (Attribute msg)
playerBoxContentAttrs =
    [ A.class "col s8" ]


playerInputAttrs : List (Attribute msg)
playerInputAttrs =
    [ sty [ height (px 30), noBottom ] ]


playerBoxesAttrs : List (Attribute msg)
playerBoxesAttrs =
    [ A.class "row", sty [ noBottom ] ]


boardAttrs : Size -> Float -> List (Attribute msg)
boardAttrs size squareSize =
    [ sty
        [ width (px (squareSize * 3))
        , marginLeft auto
        , marginRight auto
        , position relative
        , marginTop (ifWide size (px 60) (px 20))
        , left (ifWide size (px 40) (px 0))
        ]
    ]


boardRowAttrs : List (Attribute msg)
boardRowAttrs =
    [ sty [ noBottom ] ]


noBottom : Style
noBottom =
    marginBottom zero


positionAttrs : TicTacToe.Position -> Maybe Player -> Bool -> Float -> List (Attribute msg)
positionAttrs place mPlayer winning squareSize =
    let
        borderWidth_ =
            squareSize / 16

        fontSize_ =
            squareSize * 6 / 5
    in
        [ A.class "center-align"
        , sty
            ([ height (px squareSize)
             , width (px squareSize)
             , display inlineBlock
             , verticalAlign top
             , fontSize (px fontSize_)
             , borderColor (rgb 0 0 0)
             , borderStyle none
             , borderWidth (px borderWidth_)
             , lineHeight
                (px
                    (squareSize
                        - (2 * borderWidth_)
                        - if mPlayer == Just O then
                            borderWidth_
                          else
                            0
                    )
                )
             ]
                ++ positionBorders place
            )
        ]


playerNameAttrs : List (Attribute msg)
playerNameAttrs =
    [ sty [ lineHeight (px 34) ] ]


containerAttrs : List (Attribute msg)
containerAttrs =
    [ A.class "container" ]


buttonAttrs : Size -> List (Attribute msg)
buttonAttrs size =
    [ A.class ("blue " ++ ifWide size "btn" "btn-large right") ]


cardAttrs : Bool -> Bool -> List (Attribute msg)
cardAttrs myTurn iWin =
    [ A.class <|
        "card "
            ++ if myTurn then
                "teal accent-4"
               else if iWin then
                "green accent-4"
               else
                "teal darken-1"
    ]


tableAttrs : Size -> List (Attribute msg)
tableAttrs size =
    [ A.class "bordered", sty (ifWide size [ marginTop (px 60) ] []) ]


positionBorders : TicTacToe.Position -> List Style
positionBorders position =
    case position of
        TL ->
            [ borderRightStyle solid, borderBottomStyle solid ]

        TC ->
            [ borderRightStyle solid, borderBottomStyle solid, borderLeftStyle solid ]

        TR ->
            [ borderBottomStyle solid, borderLeftStyle solid ]

        CL ->
            [ borderTopStyle solid, borderRightStyle solid, borderBottomStyle solid ]

        CC ->
            [ borderTopStyle solid, borderRightStyle solid, borderBottomStyle solid, borderLeftStyle solid ]

        CR ->
            [ borderTopStyle solid, borderBottomStyle solid, borderLeftStyle solid ]

        BL ->
            [ borderTopStyle solid, borderRightStyle solid ]

        BC ->
            [ borderTopStyle solid, borderRightStyle solid, borderLeftStyle solid ]

        BR ->
            [ borderTopStyle solid, borderLeftStyle solid ]
