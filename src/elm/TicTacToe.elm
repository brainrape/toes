module TicTacToe exposing (Player(..), Position(..), Game, GameResult(..), ifX, other, empty, move, getMoves, result, winningCombos, allWinningCombos, whoseMove, winsOf)

{-|
# Tic Tac Toe game logic

@docs Player Position Game GameResult ifX other empty move getMoves result winningCombos allWinningCombos whoseMove winsOf
-}

import List exposing (length, map, filter, concat, indexedMap, filterMap)
import Tuple exposing (first, second, mapFirst)


{-| Which player?
-}
type Player
    = X
    | O


{-| A place on the game. Combinations of top, center, bottom and left, center, right
-}
type Position
    = TL
    | TC
    | TR
    | CL
    | CC
    | CR
    | BL
    | BC
    | BR


{-| A move made by a Player on a Position
-}
type alias Move =
    ( Player, Position )


{-| The state of a Game. It includes a verified list of moves. The constructor is not exported.
-}
type Game
    = Game (List Position)


{-| -}
type GameResult
    = Win Player (List (List Position))
    | Draw
    | InProgress


{-| If player is X, return one of the arguments, if it is O, then the other
-}
ifX : Player -> a -> a -> a
ifX player then_ else_ =
    case player of
        X ->
            then_

        O ->
            else_


{-| Return the other player
-}
other : Player -> Player
other player =
    ifX player O X


{-| An empty game
-}
empty : Game
empty =
    Game []


{-| Try to make a move
-}
move : Position -> Game -> Maybe Game
move place ((Game moves) as game) =
    if List.member place moves || result game /= InProgress then
        Nothing
    else
        Just (Game (place :: moves))


{-| Get the list of moves from a game
-}
getMoves : Game -> List Move
getMoves (Game moves) =
    moves
        |> List.reverse
        |> List.indexedMap (\i p -> ( whoseMove i, p ))


{-| Return the Player for the given turn number (starting from 0)
-}
whoseMove : Int -> Player
whoseMove n =
    if n % 2 == 0 then
        X
    else
        O


{-| Return the result of a game
-}
result : Game -> GameResult
result game =
    let
        winsX =
            winsOf X game

        winsO =
            winsOf O game
    in
        if not (List.isEmpty winsX) then
            Win X winsX
        else if not (List.isEmpty winsO) then
            Win O winsO
        else if (game |> getMoves |> List.length) == 9 then
            Draw
        else
            InProgress


{-| Get the list of winning combinations for a given player in a given game
-}
winsOf : Player -> Game -> List (List Position)
winsOf player game =
    game
        |> getMoves
        |> filter (first >> (\p -> p == player))
        |> map second
        |> winningCombos


{-| Return the list of winning combinations included in the given list of moves
-}
winningCombos : List Position -> List (List Position)
winningCombos moves =
    allWinningCombos
        |> map
            (\winning ->
                moves |> filter (\m -> List.member m winning)
            )
        |> filter (\xs -> length xs == 3)


{-| All possible winning combinations
-}
allWinningCombos : List (List Position)
allWinningCombos =
    [ [ TL, TC, TR ]
    , [ CL, CC, CR ]
    , [ BL, BC, BR ]
    , [ TL, CL, BL ]
    , [ TC, CC, BC ]
    , [ TR, CR, BR ]
    , [ TL, CC, BR ]
    , [ TR, CC, BL ]
    ]
