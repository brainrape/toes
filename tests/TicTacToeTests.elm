module TicTacToeTests exposing (..)

import Expect exposing (Expectation)
import Fuzz exposing (Fuzzer)
import Test exposing (..)
import TicTacToe exposing (Player(..), Position(..), Game, GameResult(..), empty, move, getMoves, result, winningCombos, winsOf)
import Maybe exposing (withDefault)


movesXWins : List Position
movesXWins =
    [ TL, CL, TC, CC, TR ]


movesDraw : List Position
movesDraw =
    [ TL, TC, CL, CC, TR, CR, BC, BL, BR ]


placeFuzzer : Fuzzer Position
placeFuzzer =
    [ TL, TC, TR, CL, CC, CR, BL, BC, BR ]
        |> List.map Fuzz.constant
        |> Fuzz.oneOf


boardFuzzer : Int -> Game -> Fuzzer Game
boardFuzzer numMoves game =
    if numMoves > 9 then
        Debug.crash "Can't have a game with more than 9 moves" empty
    else
        let
            f n g =
                if n == 0 then
                    Fuzz.constant g
                else
                    placeFuzzer
                        |> Fuzz.andThen
                            (\m ->
                                case move m g of
                                    Just g_ ->
                                        if result g_ /= InProgress then
                                            Fuzz.constant g_
                                        else
                                            f (n - 1) g_

                                    _ ->
                                        f n g
                            )
        in
            f numMoves game


gameOn : List Position -> Game -> Game
gameOn moves game =
    List.foldl (\m b -> move m b |> withDefault b) game moves


game : List Position -> Game
game moves =
    gameOn moves empty


whoWon : Game -> Maybe Player
whoWon game =
    case result game of
        Win p _ ->
            Just p

        _ ->
            Nothing


suite : Test
suite =
    describe "TicTacToe tests"
        [ test "Full Board" <|
            \_ ->
                Expect.equal
                    (getMoves (game movesDraw))
                    [ ( X, TL ), ( O, TC ), ( X, CL ), ( O, CC ), ( X, TR ), ( O, CR ), ( X, BC ), ( O, BL ), ( X, BR ) ]
        , test "Moves can not be repeated" <|
            \_ ->
                Expect.equal
                    (getMoves (game [ TL, TL, TL, TL ]))
                    [ ( X, TL ) ]
        , test "A won game should not accept more moves" <|
            \_ ->
                Expect.equal
                    (getMoves (game movesXWins))
                    [ ( X, TL ), ( O, CL ), ( X, TC ), ( O, CC ), ( X, TR ) ]
        , test "Full Game, no winner" <|
            \_ ->
                Expect.equal
                    (result (game movesDraw))
                    Draw
        , test "X Wins on Top Row" <|
            \_ ->
                Expect.equal
                    (result (game movesXWins))
                    (Win X [ [ TL, TC, TR ] ])
        , test "O Wins on middle row" <|
            \_ ->
                Expect.equal
                    (result (game [ BL, CL, TC, CC, TR, CR ]))
                    (Win O [ [ CL, CC, CR ] ])
        , fuzz placeFuzzer "No move allowed after full game" <|
            \m ->
                Expect.equal
                    (game movesDraw |> move m)
                    Nothing
        , fuzz (boardFuzzer 3 (game [ TL, TC, TR, CL, CC, BL ])) "O Can't win from here" <|
            \b ->
                Expect.notEqual
                    (whoWon b)
                    (Just O)
        , fuzz (Fuzz.tuple ( placeFuzzer, boardFuzzer 9 empty ))
            "Can't move after finshed game (won or full)"
          <|
            \( m, b ) ->
                Expect.equal
                    (move m b)
                    Nothing
        , fuzz (boardFuzzer 9 empty) "There is at most one winner" <|
            \game ->
                Expect.true "There is at most one winner"
                    (winsOf X game == [] || winsOf O game == [])
        ]
