module UpdateTests exposing (..)

import Expect exposing (Expectation)
import Test exposing (..)
import Update exposing (Msg(..), update)
import TicTacToe exposing (Player(..), Position(..), GameResult(..), result)
import TicTacToeTests exposing (placeFuzzer, movesXWins, movesDraw)
import Model exposing (Model, init)
import Dict
import List exposing (map)


run : Model -> List Msg -> Model
run model msgs =
    List.foldl update model msgs


suite : Test
suite =
    describe "Update Tests"
        [ test "Simple Game" <|
            \_ ->
                Expect.all
                    [ \m ->
                        Expect.equal
                            (m.mGame |> Maybe.map result)
                            (Just (Win X [ [ TL, TC, TR ] ]))
                    , \m ->
                        Expect.equal
                            (m.leaderboard |> Dict.get m.playerX)
                            (Just ( 1, 0, 0 ))
                    , \m ->
                        Expect.equal
                            (m.leaderboard |> Dict.get m.playerO)
                            (Just ( 0, 1, 0 ))
                    ]
                    (run init ([ NewGame ] ++ map Move movesXWins))
        , test "No moves after game is finished" <|
            \_ ->
                let
                    m =
                        (run init ([ NewGame ] ++ map Move movesXWins))
                in
                    Expect.equal
                        m
                        (update (Move BR) m)
        , test "Can't start game with bad player name" <|
            \_ ->
                Expect.equal
                    (run init [ ChangePlayerX "", NewGame ]).mGame
                    Nothing
        , test "Two Games" <|
            \_ ->
                Expect.all
                    [ \m ->
                        Expect.equal
                            (m.mGame |> Maybe.map result)
                            (Just Draw)
                    , \m ->
                        Expect.equal
                            (m.leaderboard |> Dict.get m.playerX)
                            (Just ( 0, 1, 1 ))
                    , \m ->
                        Expect.equal
                            (m.leaderboard |> Dict.get m.playerO)
                            (Just ( 1, 0, 1 ))
                    ]
                    (run init
                        (([ NewGame ] ++ map Move movesXWins)
                            ++ ([ NewGame, NewGame ] ++ map Move movesDraw)
                        )
                    )
        ]
