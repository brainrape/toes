module PlayerNameValidationTests exposing (..)

import Expect exposing (Expectation)
import Test exposing (..)
import Update exposing (validPlayers)
import Model exposing (Model, init)


p : String -> String -> Model
p x o =
    { init | playerX = x, playerO = o }


suite : Test
suite =
    describe "Player Name Validation Test"
        [ test "Valid" <|
            \_ -> Expect.true "Valid" (validPlayers (p "a" "b"))
        , test "One is empty" <|
            \_ -> Expect.false "Invalid" (validPlayers (p "" "b"))
        , test "Other is empty" <|
            \_ -> Expect.false "Invalid" (validPlayers (p "a" ""))
        , test "Both are empty" <|
            \_ -> Expect.false "Invalid" (validPlayers (p "" ""))
        ]
